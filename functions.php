<?php

/**
* загружаемые скрипты и стили
*/
function load_style_script(){
    wp_register_script('es5-shim', get_template_directory_uri() . "/libs/html5shiv/es5-shim.min.js",false,false,true);
    wp_register_script('html5shiv', get_template_directory_uri() . "/libs/html5shiv/html5shiv.min.js",false,false,true);
    wp_register_script('html5shiv-printshiv', get_template_directory_uri() . "/libs/html5shiv/html5shiv-printshiv.min.js",false,false,true);
    wp_register_script('respond', get_template_directory_uri() . "/libs/respond/respond.min.js",false,false,true);

    wp_register_script('jquery1111', get_template_directory_uri() . "/libs/jquery/jquery-1.11.1.min.js",false,false,true);
    wp_register_script('mousewheel', get_template_directory_uri() . "/libs/jquery-mousewheel/jquery.mousewheel.min.js",false,false,true);
    wp_register_script('fancybox', get_template_directory_uri() . "/libs/fancybox/jquery.fancybox.pack.js",false,false,true);
    wp_register_script('waypoints', get_template_directory_uri() . "/libs/waypoints/waypoints-1.6.2.min.js",false,false,true);
    wp_register_script('scrollto', get_template_directory_uri() . "/libs/scrollto/jquery.scrollTo.min.js",false,false,true);
    wp_register_script('owl-carousel', get_template_directory_uri() . "/libs/owl-carousel/owl.carousel.min.js",false,false,true);
    wp_register_script('countdown-pl', get_template_directory_uri() . "/libs/countdown/jquery.plugin.js",false,false,true);
    wp_register_script('countdown-min', get_template_directory_uri() . "/libs/countdown/jquery.countdown.min.js",false,false,true);
    wp_register_script('countdown-ru', get_template_directory_uri() . "/libs/countdown/jquery.countdown-ru.js",false,false,true);
    wp_register_script('navigation', get_template_directory_uri() . "/libs/landing-nav/navigation.js",false,false,true);
    wp_register_script('common', get_template_directory_uri() . "/js/common.js",false,false,true);

    wp_enqueue_script('jquery1111');
    wp_enqueue_script('mousewheel');
    wp_enqueue_script('fancybox');
    wp_enqueue_script('waypoints');
    wp_enqueue_script('scrollto');
    wp_enqueue_script('owl-carousel');
    wp_enqueue_script('countdown-pl');
    wp_enqueue_script('countdown-min');
    wp_enqueue_script('countdown-ru');
    wp_enqueue_script('navigation');
    wp_enqueue_script('common');

	wp_enqueue_style('bootstrap-grid', get_template_directory_uri() . '/libs/bootstrap/bootstrap-grid-3.3.1.min.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/libs/font-awesome-4.2.0/css/font-awesome.min.css');
	wp_enqueue_style('fancybox', get_template_directory_uri() . '/libs/fancybox/jquery.fancybox.css');
	wp_enqueue_style('owl', get_template_directory_uri() . '/libs/owl-carousel/owl.carousel.css');
	wp_enqueue_style('countdown', get_template_directory_uri() . '/libs/countdown/jquery.countdown.css');
	wp_enqueue_style('fonts', get_template_directory_uri() . '/css/fonts.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css');
	wp_enqueue_style('media', get_template_directory_uri() . '/css/media.css');
}

/**
* загружаем скрипты и стили
*/
add_action('wp_enqueue_scripts', 'load_style_script');

/**
* поддержка миниатюр
*/
add_theme_support('post-thumbnails');
set_post_thumbnail_size(180,180);

/**
* добавляем виджеты
*/
register_sidebar(array(
				'name' => 'Меню',
				'id' => 'menu_header',
				'before_widget' => '',
				'after_widget' => ''));
